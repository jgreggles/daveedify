<?php

namespace App\Controller;

use App\Entity\Link;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Psr\Log\LoggerInterface;
use URL\Normalizer;

class HomepageController extends AbstractController
{
    private $em;

    private $logger;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * @Route("/", name="app_homepage")
     */
    public function index(Request $request): Response
    {
        // creates a task object and initializes some data for this example
        $link = new Link();
        $link->setShortUri('');

        $form = $this->createFormBuilder($link)
            ->add('uri', UrlType::class, [
                'attr' => [
                    'placeholder' => 'https://example.com'
                ]
            ])
            ->add('save', SubmitType::class, ['label' => 'daveedify!'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Link $link */
            $link = $form->getData();

            $normalizedUri = (new Normalizer($link->getUri()))->normalize();

            $linkRepo = $this->em->getRepository(Link::class);
            $existingLink = $linkRepo->findOneBy(['uri' => $normalizedUri]);

            $link->setUri($normalizedUri);

            if (!is_null($existingLink)) {
                $this->addFlash('success', $existingLink->getShortUri());
            } else {
                $this->em->persist($link);
                $this->em->flush();
                $this->addFlash('success', $link->getShortUri());
            }

            return $this->redirectToRoute('app_homepage');
        }


        return $this->render('homepage/index.html.twig', [
            'controller_name' => 'HomepageController',
            'form' => $form->createView()
        ]);
    }
}
