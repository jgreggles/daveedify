<?php

namespace App\Controller;

use App\Entity\Link;
use App\Entity\LinkHit;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class LinkController extends AbstractController
{
    private $em;

    /** @var HttpClientInterface $httpClient */
    private $httpClient;

    private $logger;

    public function __construct(EntityManagerInterface $em, HttpClientInterface $httpClient, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    /**
     * @Route("/{shortUri}", name="app_link")
     */
    public function index($shortUri, Request $request): Response
    {
        $linkRepo = $this->em->getRepository(Link::class);

        $link = $linkRepo->findOneBy([
            'shortUri' => $shortUri
        ]);

        if ($link) {
            $hit = (new LinkHit())
                ->setLink($link)
                ->setIp($request->getClientIp())
                ->setTime(new \DateTime());

            $this->em->persist($hit);
            $this->em->flush();

            $fact = $this->buildFact();

            return $this->render('link/index.html.twig', [
                'controller_name' => 'LinkController',
                'fact' => $fact,
                'link' => $link
            ]);
        }

        $this->addFlash('error', 'The link was not found!');

        return $this->redirectToRoute('app_homepage');
    }

    private function buildFact()
    {
        $year = rand(1000, 2022);
        $response = $this->httpClient->request('GET', "https://api.api-ninjas.com/v1/historicalevents?year=${year}", [
            'headers' => [
                'X-Api-Key' => $this->getParameter('apiNinjaKey')
            ]
        ]);

        $this->logger->info('API Response Code: ' . $response->getStatusCode());
        $this->logger->info('API Response: ' . $response->getContent());

        $fact = [
            'event' => 'There were 31 series of \'Last of the Summer Wine\'',
            'date' => new \DateTime()
        ];

        $content = $response->toArray();

        if (count($content) > 0) {
            $fact = [
                'event' => $content[0]['event'],
                'date' => new \DateTime($content[0]['year'].'-'.$content[0]['month'].'-'.$content[0]['day'])
            ];
        }

        return $fact;
    }
}
