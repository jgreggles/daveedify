<?php

namespace App\Entity;

use App\Repository\LinkRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=LinkRepository::class)
 */
class Link
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     * @Assert\Url
     */
    private $uri;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $shortUri;

    /**
     * @ORM\OneToMany(targetEntity=LinkHit::class, mappedBy="link", orphanRemoval=true)
     */
    private $linkHits;

    public function __construct()
    {
        $this->linkHits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUri(): ?string
    {
        return $this->uri;
    }

    public function setUri(string $uri): self
    {
        $this->uri = $uri;

        return $this;
    }

    public function getShortUri(): ?string
    {
        return $this->shortUri;
    }

    public function setShortUri(string $shortUri): self
    {
        $this->shortUri = $shortUri;

        return $this;
    }

    /**
     * @return Collection<int, LinkHit>
     */
    public function getLinkHits(): Collection
    {
        return $this->linkHits;
    }

    public function addLinkHit(LinkHit $linkHit): self
    {
        if (!$this->linkHits->contains($linkHit)) {
            $this->linkHits[] = $linkHit;
            $linkHit->setLink($this);
        }

        return $this;
    }

    public function removeLinkHit(LinkHit $linkHit): self
    {
        if ($this->linkHits->removeElement($linkHit)) {
            // set the owning side to null (unless already changed)
            if ($linkHit->getLink() === $this) {
                $linkHit->setLink(null);
            }
        }

        return $this;
    }
}
